variable "ami_us_east_2" {}
variable "instance_type" {}
variable "public_key_location" {}
variable "private_key_location" {}
variable "env_prefix" {
  description = "deployment environment"
  default     = "dev-env"
}
variable "cidr_blocks" {}
variable "avail_zone_a" {}
variable "avail_zone_b" {}
variable "my_ip" {
  default = "173.174.75.38"
}
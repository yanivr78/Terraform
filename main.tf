module "myapp-subnet" {
  source                 = "./modules/subnet"
  cidr_blocks            = var.cidr_blocks
  env_prefix             = var.env_prefix
  avail_zone_a           = var.avail_zone_a
  avail_zone_b           = var.avail_zone_b
  vpc_id                 = aws_vpc.development-vpc.id
  default_route_table_id = aws_vpc.development-vpc.default_route_table_id
}
module "myapp-webserver" {
  source               = "./modules/webserver"
  avail_zone           = var.avail_zone_a
  instance_type        = var.instance_type
  my_ip                = var.my_ip
  private_key_location = var.private_key_location
  public_key_location  = var.public_key_location
  subnet               = module.myapp-subnet.subnet-1.id
  vpc_id               = aws_vpc.development-vpc.id
}

resource "aws_vpc" "development-vpc" {
  cidr_block = var.cidr_blocks[0].cidr_block
  tags = {
    Name : var.env_prefix
    vpc-env : var.cidr_blocks[0].name
  }
}


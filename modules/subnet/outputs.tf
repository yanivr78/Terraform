output "subnet-1" {
  value = aws_subnet.dev-subnet-1
}

output "subnet-2" {
  value = aws_subnet.dev-subnet-2
}

#!/bin/bash
sudo yum update -y && sudo yum install docker -y
sudo systemctl start docker
sudo usermod -aG docker ec2-user
sudo systemctl restart docker # needs to run to apply usermod
sudo docker run -d -p 8080:80 nginx # without -d terraform will get stuck in a loop
